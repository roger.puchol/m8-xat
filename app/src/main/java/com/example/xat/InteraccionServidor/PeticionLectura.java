package com.example.xat.InteraccionServidor;

/* **************

Esta clase sirve para leer datos del servidor sin necesidad de pasar datos (metodo GET). Solo es necesario pasarle la ruta del archivo .php del que leer y un listener del tipo deseado para recoger los datos

Sintaxis:

    PeticionLectura.Lee(context, ruta, new RespuestaServidor<T>); Devuelve una lista manipulable de tipo T a través de el objeto RespuestaServidor (es un listener)

Ejemplo (leer usuarios):

///
Activity activity = this; //Si llamas a PeticionLectura.Lee desde un listener debes poner esto fuera de el
RespuestaServidor<Usuario> respuesta = new RespuestaServidor<Usuario>() {

                @Override
                public void acabarProceso(List<Usuario> output) {
                    //Código para los datos recibidos aquí
                }
            }


    PeticionLectura.lee(activity, SingletonDades.getInstance(activity.getApplicationContext()).get_all_users, respuesta};
///

Roger: ... siento que se me haya ido tanto la olla con este código

*************** */

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.xat.Modelos.ModeloBase;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.ParameterizedType;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;


public class PeticionLectura<T extends ModeloBase> extends AsyncTask<Object, Void, List<T>> {


    // Mostramos un ProgressDialog para hacer mas amena la esper (Esto es prescindible--> Hay que inicializar en el onPreExecute())
    private ProgressDialog pd;
    private Activity activity;
    private URL ruta;
    private RespuestaServidor<T> respuestaServidor;
    Class<T> clase;

    HttpURLConnection con;

    //Funcion general de lectura. Solo requiere una ruta a la que apuntar (las rutas se pueden encontrar como variables de SingletonDades, por ejemplo get_all_users)
    //Sintaxis: PeticionLectura.Lee(context, ruta, new RespuestaServidor<T>); Devuelve una lista manipulable de tipo T a través de el objeto RespuestaServidor (es un listener)
    //Ejemplo (leer usuarios): PeticionLectura.lee(this, SingletonDades.getInstance(getApplicationContext()).get_all_users, new RespuestaServidor<Usuario>(){...})
    public static <T extends ModeloBase> void Lee(@NonNull Activity activity, String ruta, @NonNull RespuestaServidor<T> respuesta) {

        try {
            new PeticionLectura(activity, new URL(ruta), respuesta).execute();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    /* Constructor */
    private PeticionLectura(Activity activity, URL url, RespuestaServidor<T> respuestaServidor) {
        this.pd = new ProgressDialog(activity); // Necesitamos el Activity en el constuctor para que funcione el ProgessDialog
        this.activity = activity;
        this.ruta = url;
        this.respuestaServidor = respuestaServidor;
        //Què és el que fa aquí?

        //Agafa classe PeticióLectura<T> -> agafa AsyncTask<String,Void,List<T>> -> agafa List<T> -> agafa T

        //Tot això ens permet obtenir l'objecte de tipus <T extends ModeloBase> que ens permet llegir l'objecte que sigui mentre estengui la classe ModeloBase sense haver de passar gaires paràmetres.
        try {
            //this.clase = ((Class) ((ParameterizedType) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[2]).getActualTypeArguments()[0]); //Es probable que peti aqui
            clase = (Class<T>) ((ParameterizedType) respuestaServidor.getClass().getGenericInterfaces()[0]).getActualTypeArguments()[0];
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onPreExecute() {
        // Barra de progreso

        this.pd.setMessage("Obteniendo datos....");
        this.pd.setIndeterminate(false);
        this.pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        this.pd.setCancelable(false);
        this.pd.show();
    }


    @Override
    protected List<T> doInBackground(Object... urls) {
        // DEvolveremos una lista de usuarios que llemos del servicio web
        List<T> llista = null;

        // Establecemos conexión sobre la variable global con
        // y sobre el primero de los valores de urls
        // Es obligado que este en un try/catch
        try {

            con = (HttpURLConnection) ruta.openConnection();

            // Parmatreizamos los tiempos de espera y de lectura. En milisegundos
            con.setConnectTimeout(15000);
            con.setReadTimeout(10000);
//            Log.i("nico", urls[0].toString());
            // Ejecturamos conexión y evaluamos resultado

            int codEstadoCon = con.getResponseCode();

            if (codEstadoCon == 200) {

                // Provocamos un retraso para que se vea efecto del ProgessDialog  ->> mejor no poner Sleeps ni toast  en tareas asincronas,
               /* for (int j = 0; j < 9999; j++) {
                    int i = 0;
                    while (i < 9999) {
                        i = i + 1;
                    }

                }*/
                // Tenemos que parsear los datos JSON que nos legan a traves de la conexión
                InputStream in = new BufferedInputStream(con.getInputStream());
                // Vamos parseando la entrada usando Gson a usuarios
                llista = GsonParser.leer(in, clase);

            } else {
                Toast.makeText(activity, "Error en la lectura d'objectes tipus" + new TypeToken<T>() {
                }.getType().toString(), Toast.LENGTH_SHORT).show();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // Desconectamos en cualquera de los casos la conneción
            con.disconnect();
        }
        return llista;
    }


    @Override
    protected void onPostExecute(List<T> lista) {
        if (lista != null) {
            respuestaServidor.acabarProceso(lista);
        } else {
            Toast.makeText(activity.getApplicationContext(), "Error parseando datos", Toast.LENGTH_LONG).show();
        }


        // Quitamos progress
        if (pd.isShowing()) {
            pd.dismiss();
        }

    }
}