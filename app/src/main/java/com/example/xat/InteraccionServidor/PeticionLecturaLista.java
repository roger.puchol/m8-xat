package com.example.xat.InteraccionServidor;

/* **************
Esta clase sirve para actualizar directamente adaptadores de listas que extiendan AdaptadorBase.

Aquesta classe està obsoleta i s'hauria d'utilitzar PeticionLectura en el seu lloc.


*************** */

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.xat.Modelos.AdaptadorBase;
import com.example.xat.Modelos.ModeloBase;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;


public class PeticionLecturaLista<T extends ModeloBase, X extends AdaptadorBase<T>> extends AsyncTask<Object, Void, List<T>> {


    // Mostramos un ProgressDialog para hacer mas amena la esper (Esto es prescindible--> Hay que inicializar en el onPreExecute())
    private ProgressDialog pd;
    private Activity activity;
    private X adaptador;
    private URL ruta;

    private HttpURLConnection con;

    //Funcion general de lectura. Solo requiere que la clase que queremos leer tenga una version de AdaptadorBase creada
    //Sintaxis: PeticionLecturaLista.Lee<Clase,Adaptador>(this, listView, adaptador); Asigna la lista de usuarios al ListView usando el adaptador proporcionado
    public static <T extends ModeloBase, X extends AdaptadorBase<T>> void Lee(@NonNull Activity activity, String ruta, @NonNull ListView listView, @NonNull X adaptador) {
        try {
            if (listView.getAdapter() == null) listView.setAdapter(adaptador);
            new PeticionLecturaLista(activity, adaptador, new URL(ruta)).execute();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            Toast.makeText(activity, "Hi ha hagut un error al intentar enviar la petició.", Toast.LENGTH_LONG).show();
        }
    }

    /* Constructor */
    private PeticionLecturaLista(Activity activity, X adaptador, URL url) {

        this.pd = new ProgressDialog(activity); // Necesitamos el Activity en el constuctor para que funcione el ProgessDialog
        this.activity = activity;
        this.adaptador = adaptador;
        ruta = url;
    }


    @Override
    protected void onPreExecute() {
        // Barra de progreso

        this.pd.setMessage("Obteniendo datos....");
        this.pd.setIndeterminate(false);
        this.pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        this.pd.setCancelable(false);
        this.pd.show();
    }


    @Override
    protected List<T> doInBackground(Object... urls) {
        // DEvolveremos una lista de usuarios que llemos del servicio web
        List<T> llista = null;

        // Establecemos conexión sobre la variable global con
        // y sobre el primero de los valores de urls
        // Es obligado que este en un try/catch
        try {

            con = (HttpURLConnection) ruta.openConnection();

            // Parmatreizamos los tiempos de espera y de lectura. En milisegundos
            con.setConnectTimeout(15000);
            con.setReadTimeout(10000);
//            Log.i("nico", urls[0].toString());
            // Ejecturamos conexión y evaluamos resultado

            int codEstadoCon = con.getResponseCode();

            if (codEstadoCon == 200) {

                // Provocamos un retraso para que se vea efecto del ProgessDialog  ->> mejor no poner Sleeps ni toast  en tareas asincronas,
               /* for (int j = 0; j < 9999; j++) {
                    int i = 0;
                    while (i < 9999) {
                        i = i + 1;
                    }

                }*/
                // Tenemos que parsear los datos JSON que nos legan a traves de la conexión
                InputStream in = new BufferedInputStream(con.getInputStream());
                // Vamos parseando la entrada usando Gson a usuarios
                llista = GsonParser.leer(in, adaptador.ejemploClase);

            } else {
                Toast.makeText(activity, "Error en la lectura d'objectes tipus" + adaptador.ejemploClase.getCanonicalName(), Toast.LENGTH_SHORT).show();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // Desconectamos en cualquera de los casos la conneción
            con.disconnect();
        }
        return llista;
    }


    @Override
    protected void onPostExecute(List<T> lista) {
        // super.onPostExecute(usuarios);
        // Una vez hemos ejecutado la lectura de los objetos json tenimos que parsearlos al adatador
        if (lista != null && !lista.isEmpty()) {
            adaptador.actualizaDatos(lista);
        } else {
            Toast.makeText(activity.getApplicationContext(), "Error parseando datos", Toast.LENGTH_LONG).show();
        }


        // Quitamos progress
        if (pd.isShowing()) {
            pd.dismiss();
        }

    }
}