package com.example.xat.InteraccionServidor;

/* **************
ESta clase sirve para la inserción de registros en la BBDD remota usando el servicioweb ya creao en el servidor

Tenemos que usar la  libreria Volley -->        implementation 'com.android.volley:volley:1.1.1'  y tenemos que añadir esa line a build.grade
Esto es para poer usar la StringRequest.
Volley es una libreria desarrollada por Google para  optimizar el envio de peticiones Http desde Android a servidores extenos.
https://developer.android.com/training/volley


*************** */

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.example.xat.Modelos.Mensajes.Mensajes;
import com.example.xat.Modelos.Usuarios.Usuario;
import com.example.xat.SingletonDades;

import java.util.HashMap;
import java.util.Map;


public class PeticionPost extends StringRequest {
    // Definimos la put_usuario del archivo php para escribir en la BBDD
    //



    private Map<String, String> parametros;  // parametros que contentdra los valores del registro

    public static PeticionPost CrearPeticion(String rutaPHP, Map<String, String> datos, Response.Listener<String> listener) {
        return new PeticionPost(datos, listener, rutaPHP);
    }

    //El propio nombre lo dice
    public static PeticionPost Login(String user_name, String user_pass, Response.Listener<String> listener) { //TODO Testejar amb el servidor quan estigui el PHP i l'activity
        Map<String, String> datos = new HashMap<String, String>();
        datos.put("user_name", user_name);
        datos.put("user_pass", user_pass);
        return new PeticionPost(datos, listener, SingletonDades.login);
    }

    //Nuevo usuario, el listener recibe un booleano indicando si ha podido registrar al usuario o no
    public static PeticionPost RegistraUsuario(String user_name, String user_pass, Response.Listener<String> listener) {
        Map<String, String> datos = new HashMap<String, String>();
        datos.put("user_name", user_name);
        datos.put("user_pass", user_pass);
        return new PeticionPost(datos, listener, SingletonDades.put_user);
    }

    //Actualizar nombre, contraseña y/o imagen
    public static PeticionPost ActualizaUsuario(boolean actualiza, Usuario usuario, Response.Listener<String> listener) { //TODO Testejar amb el servidor quan estigui el PHP i l'activity
        Map<String, String> datos = new HashMap<String, String>();

        datos.put("user_id", usuario.getUser_id() + "");
        datos.put("user_name", usuario.getUser_name());
        datos.put("user_pass", usuario.getUser_pass());
        //datos.put("user_image",usuario.getUser_image());

        return new PeticionPost(datos, listener, SingletonDades.update_user);
    }

    //Ahora que podemos hacer login, falta crear la activity de seleccion de chat y la de chat en si mismo. Haré también una petición para recibir mensajes a partir de una fecha proporcionada
    public static PeticionPost EnviaMensaje(Mensajes mensaje) { //TODO Testejar amb el servidor quan estigui el PHP i l'activity
        Map<String, String> datos = new HashMap<String, String>();

        datos.put("usuari_remitent", mensaje.getUsuari_remitent() + "");
        datos.put("text", mensaje.getText());

        return new PeticionPost(datos, null, SingletonDades.post_message);
    }

    //Envia mensaje con listener para continuar el codigo despues de que el servidor lo reciba
    public static PeticionPost EnviaMensaje(Mensajes mensaje, Response.Listener<String> listener) { //TODO Testejar amb el servidor quan estigui el PHP i l'activity
        Map<String, String> datos = new HashMap<String, String>();

        datos.put("usuari_remitent", mensaje.getUsuari_remitent() + "");
        datos.put("text", mensaje.getText());

        return new PeticionPost(datos, listener, SingletonDades.post_message);
    }

    //Constructor general
    private PeticionPost(Map<String, String> datos, Response.Listener<String> listener, String ruta) {
        super(Method.POST, ruta, listener, null);
        parametros = datos;
    }


    // Sobreescribimos map  para asegurarnos que nos devuelve la información de uestros parametros
    @Override
    protected Map<String, String> getParams() {
        return parametros;
    }
}
