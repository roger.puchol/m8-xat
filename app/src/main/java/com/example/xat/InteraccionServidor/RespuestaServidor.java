package com.example.xat.InteraccionServidor;

import com.example.xat.Modelos.ModeloBase;

import java.util.List;

public interface RespuestaServidor<T extends ModeloBase> {
    void acabarProceso(List<T> output);
}
