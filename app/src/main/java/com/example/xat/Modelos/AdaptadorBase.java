package com.example.xat.Modelos;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.List;


//LOS OBJETOS USADOS CON ESTE TIPO DE ADAPTADOR DEBEN TENER UN CONSTRUCTOR VACÍO Y EL ADAPTADOR DEBE TENER UN OBJETO DE REFERENCIA (aunque tenga datos no válidos)


public abstract class AdaptadorBase<T extends ModeloBase> extends ArrayAdapter<T> {

    protected Context context;
    protected List<T> llista;
    public Class<T> ejemploClase; //S'utilitza a PeticionLecturaLista per cridar al GsonParser (l'ideal seria passar a fer servir unicament PeticionLectura tant per llistes com objectes individuals)

    public AdaptadorBase(Context context, List<T> objects) {
        super(context, 0, objects);
    }

    public abstract View getView(int position, View convertView, ViewGroup parent);

    public abstract void actualizaDatos(List<T> datos);




    //Función de nico, nos devuelve el R.id.nombre de lo que le pidamos
    private int convertirRutaEnId(String nombre) {
        Context context = getContext();
        return context.getResources()
                .getIdentifier(nombre, "drawable", context.getPackageName());
    }

}
