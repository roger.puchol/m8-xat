package com.example.xat.Modelos.Mensajes;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.xat.Modelos.AdaptadorBase;
import com.example.xat.R;

import java.util.List;

public class AdaptadorMensajes extends AdaptadorBase<Mensajes> {

    Mensajes m = new Mensajes();

    public AdaptadorMensajes(Context context, List<Mensajes> objects) {
        super(context, objects);
        llista = objects;
        this.context = context;
        ejemploClase = Mensajes.class;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        if(v==null){
            LayoutInflater li= (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v=li.inflate(R.layout.item_chat, parent,false);
        }
        m=llista.get(position);

        TextView nom = v.findViewById(R.id.nombreMensaje);
        TextView missatge = v.findViewById(R.id.mensajeTexto);
        TextView data = v.findViewById(R.id.mensajeFecha);

        nom.setText(m.getUsuari_remitent_name());
        missatge.setText(m.getText());
        data.setText(m.getData_hora());

        return v;

    }

    @Override
    public void actualizaDatos(List<Mensajes> datos) {
        llista.clear();
        llista.addAll(datos);
        notifyDataSetChanged();
    }

    public void afegirMissatges(List<Mensajes> dades){
        llista.addAll(dades);
        notifyDataSetChanged();
    }

    public void afegirMissatges(Mensajes dades){
        llista.add(dades);
        notifyDataSetChanged();
    }
}
