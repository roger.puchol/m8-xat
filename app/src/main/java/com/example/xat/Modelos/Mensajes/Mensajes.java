package com.example.xat.Modelos.Mensajes;

import com.example.xat.Modelos.ModeloBase;
import com.example.xat.Modelos.Usuarios.Usuario;
import com.example.xat.SingletonDades;

public class Mensajes extends ModeloBase {

    public static final String get_all_this = "/get_mensaje.php";

    private int id_missatge;
    private int usuari_remitent;
    private String usuari_remitent_name;
    private String text;
    private String data_hora;

    public Mensajes(int id_missatge, int usuari_remitent, String usuari_remitent_name, String text, String data_hora) {
        this(true);
        this.id_missatge = id_missatge;
        this.usuari_remitent = usuari_remitent;
        this.usuari_remitent_name = usuari_remitent_name;
        this.text = text;
        this.data_hora = data_hora;
    }

    private Mensajes(boolean bool) {
        super();
    }

    public Mensajes() {}

    public int getId_missatge() {
        return id_missatge;
    }

    public void setId_missatge(int id_missatge) {
        this.id_missatge = id_missatge;
    }

    public int getUsuari_remitent() {
        return usuari_remitent;
    }

    public String getUsuari_remitent_name(){
        return usuari_remitent_name;
    }

    public void setUsuari_remitent(int usuari_remitent) {
        this.usuari_remitent = usuari_remitent;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getData_hora() {
        return data_hora;
    }

    public void setData_hora(String data_hora) {
        this.data_hora = data_hora;
    }
}
