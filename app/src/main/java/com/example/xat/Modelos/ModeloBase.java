package com.example.xat.Modelos;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public abstract class ModeloBase {

    protected ModeloBase() {
    }

    public HashMap<String, String> toMap(){
        HashMap<String, String> map = new HashMap<>();
        for (Field field : this.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            try { map.put(field.getName(), field.get(this).toString()); } catch (Exception e) { }
        }
        return map;
    }

}
