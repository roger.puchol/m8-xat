package com.example.xat.Modelos.Usuarios;

import com.example.xat.Modelos.ModeloBase;

import java.util.HashMap;
import java.util.Map;

public class Usuario extends ModeloBase {

    public static final String get_all = "/get_usuarios.php";

    private int user_id;
    private String user_name;
    private String user_pass;
    private String last_seen;
    private int user_image;


    public Usuario(int user_id, String nombre, String apellido, String last_seen) {
        super();
        this.user_id = user_id;
        this.user_name = nombre;
        this.user_pass = apellido;
        this.last_seen = last_seen;
        this.user_image = -1;
    }

    public Usuario(int user_id, String nombre, String apellido, String last_seen, String user_image) {
        super();
        this.user_id = user_id;
        this.user_name = nombre;
        this.user_pass = apellido;
        this.last_seen = last_seen;
        this.user_image = -1;
    }

    public Usuario() {
        super();
        user_id = -1;
        user_name = null;
        user_pass = null;
        last_seen = null;
        user_image = -1;
    }

    public int getUser_id() {
        return user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public String getUser_pass() {
        return user_pass;
    }

    public String getLast_seen() {
        return last_seen;
    }

    public int getUser_image() {
        return user_image;
    }



}
