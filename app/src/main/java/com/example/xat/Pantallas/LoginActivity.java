package com.example.xat.Pantallas;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.example.xat.InteraccionServidor.PeticionPost;
import com.example.xat.R;

import org.json.JSONException;
import org.json.JSONObject;

import static android.widget.Toast.LENGTH_LONG;

public class LoginActivity extends AppCompatActivity {

    public static int user_id;
    EditText txtUser, txtPassword;
    Button btnLogin, btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        txtUser = findViewById(R.id.txtUser);
        txtPassword = findViewById(R.id.txtPassword2);
        btnLogin = findViewById(R.id.btnLogin);
        btnRegister = findViewById(R.id.btnRegister);

        final Context context = this;

        btnLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String username = txtUser.getText().toString();
                String password = txtPassword.getText().toString();
                if(!username.equals("") && !password.equals("")){
                    Response.Listener<String> respuesta = new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // Aqui ponemos lo que queremos que haga con la respuesta (response) del servidor a nuestra peticion
                            // Pensemos que estamos dando de alta un nuevo registro.
                            // El php envia un array en formato json, por lo tanto tenemos que tratar la información en json
                            try {
                                JSONObject jsonRepuesta = new JSONObject(response);
                                Log.i("holaholita", response);
                                boolean ok = jsonRepuesta.getBoolean("success"); // El php nos responde un array con un valor "success". Aquó lo rescatamos- Es un  boolea
                                //if(ok) Log.i("holaholita", "true");
                                //else Log.i("holaholita", "false");
                                if (ok) {
                                    user_id = jsonRepuesta.getInt("user_id");
                                    Log.i("holaholita", ""+user_id);
                                    Toast.makeText(getApplicationContext(), "Login vàlid", LENGTH_LONG).show();
                                    Intent intent = new Intent(getApplicationContext(), MensajesActivity.class).putExtra("nom", txtUser.getText().toString());
                                    startActivityForResult(intent,2);
                                    //TODO falta la activity a la que irás tras hacer el login
                                    //Intent intent = new Intent(getApplicationContext(), );
                                    //startActivityForResult(intent,2);
                                } else {
                                    Toast.makeText(getApplicationContext(), "Login invàlid", LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    PeticionPost r = PeticionPost.Login(username, password, respuesta);
                    RequestQueue cola = Volley.newRequestQueue(context);
                    cola.add(r);
                }else{
                    Toast.makeText(getApplicationContext(), "Cal omplir tots els camps", LENGTH_LONG).show();
                }
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivityForResult(intent, 1);
            }
        });
    }

}
