package com.example.xat.Pantallas;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.example.xat.EditarPrefs;
import com.example.xat.InteraccionServidor.PeticionLectura;
import com.example.xat.InteraccionServidor.PeticionPost;
import com.example.xat.InteraccionServidor.RespuestaServidor;
import com.example.xat.Modelos.Mensajes.AdaptadorMensajes;
import com.example.xat.Modelos.Mensajes.Mensajes;
import com.example.xat.R;
import com.example.xat.SingletonDades;

import java.util.ArrayList;
import java.util.List;

public class MensajesActivity extends AppCompatActivity {

    SingletonDades dades;

    ArrayList<Mensajes> listaMensajes = new ArrayList<>();
    AdaptadorMensajes adaptadorMensajes;
    ListView lvMensajes;
    MediaPlayer mediaPlayer;

    Button enviar, refresh;
    EditText text;

    TextView nom;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_xat);
        dades = SingletonDades.getInstance(getApplicationContext());

        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

        Intent i = getIntent();
        nom = findViewById(R.id.txtNom);
        String nomUsuari = i.getStringExtra("nom");
        nom.setText(nomUsuari);


        final Context context = this;

        adaptadorMensajes = new AdaptadorMensajes(getApplicationContext(), listaMensajes);
        lvMensajes = findViewById(R.id.lvMissatges);
        refresh = findViewById(R.id.btnActualitzar);

        lvMensajes.setAdapter(adaptadorMensajes);

        actualizaMensajes();

        //PeticionLecturaLista.Lee(this,dades.getServidor()+Mensajes.get_all_this, lvMensajes, adaptadorMensajes);

        //TODO Montar activity de mensajes usando un ListView

        enviar = findViewById(R.id.btnEnviar);
        text = findViewById(R.id.txtMissatge);

        final Response.Listener<String> listenerActualizacion = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                actualizaMensajes();
            }
        };

        enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String contenido = text.getText().toString();
                Mensajes mensaje = new Mensajes();
                mensaje.setText(contenido);
                mensaje.setUsuari_remitent(LoginActivity.user_id);
                PeticionPost r = PeticionPost.EnviaMensaje(mensaje, listenerActualizacion);
                RequestQueue cola = Volley.newRequestQueue(context);
                cola.add(r);
                mediaPlayer = MediaPlayer.create(MensajesActivity.this, R.raw.laser);
                mediaPlayer.start();
            }
        });

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actualizaMensajes();
                mediaPlayer = MediaPlayer.create(MensajesActivity.this, R.raw.laser);
                mediaPlayer.start();
            }
        });

    }

    public void actualizaMensajes(){
        PeticionLectura.Lee(this, SingletonDades.get_all_messages, new RespuestaServidor<Mensajes>() {
            @Override
            public void acabarProceso(List<Mensajes> output) {
                adaptadorMensajes.actualizaDatos(output);
                lvMensajes.setAdapter(adaptadorMensajes);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        LinearLayout ll = findViewById(R.id.ll);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if(prefs.getString("lpBackground", "<unset>").equals("backgroundBlue")){
            ll.setBackgroundResource(R.drawable.background2);
        }else if(prefs.getString("lpBackground", "<unset>").equals("backgroundRed")){
            ll.setBackgroundResource(R.drawable.backgroundsolar);
        }else{
            ll.setBackgroundResource(R.drawable.background2);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.barramenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Intent preferenceIntent = new Intent(MensajesActivity.this, EditarPrefs.class);

        startActivityForResult(preferenceIntent, 0);

        return true;
    }


}
