package com.example.xat.Pantallas;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.example.xat.InteraccionServidor.PeticionLectura;
import com.example.xat.InteraccionServidor.RespuestaServidor;
import com.example.xat.Modelos.AdaptadorBase;
import com.example.xat.InteraccionServidor.GsonParser;
import com.example.xat.InteraccionServidor.PeticionPost;
import com.example.xat.InteraccionServidor.PeticionLecturaLista;
import com.example.xat.R;
import com.example.xat.SingletonDades;
import com.example.xat.Modelos.Usuarios.Usuario;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static android.widget.Toast.*;


public class PruebasActivity extends AppCompatActivity {
    /*
      Variables globales
       */
    ListView lista;
    AdaptadorBase adaptador;

    SingletonDades dades;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pruebas);
        dades = SingletonDades.getInstance(getApplicationContext());
        // requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);


        // Accedemos a la lista de usuarios del layout
        lista = (findViewById(R.id.lvUsuarios));

        //adaptador = new AdaptadorUsuarios(this, new ArrayList<Usuario>());

        lista.setAdapter(adaptador);

         /*  **********************************
        // Inici   Suponemos que tenemos el JSON  en un fichero.

        String jsonString = "";
        try {
            InputStream stream = getAssets().open("usuarios.json");
            int size = stream.available();
            byte[] buffer = new byte[size];
            stream.read(buffer);
            stream.close();
            jsonString = new String(buffer);
        } catch (IOException e) {
            Log.i("nico", "Error al leer el archivo!, " + e.getMessage());
        }
        // YA tenemos leido el fichero

        List<Usuario> usuarios = new ArrayList<>();
        try {
            JSONArray users= new JSONArray(jsonString);
            for (int i=0; i<users.length(); i++) {
                JSONObject user=users.getJSONObject(i);
                Usuario newUser = new Usuario(user.getString("nombre").toString(),
                                              user.get("apellido").toString(),
                                               user.getDouble("latitud"),
                                                user.getDouble("longitud") )  ;
                usuarios.add(newUser);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (usuarios!=null) {
            adaptador = new AdaptadorUsuarios(getApplicationContext(),usuarios);
            lista.setAdapter(adaptador);
        }else {
            Toast.makeText(getApplicationContext(), "Error parseando datos", Toast.LENGTH_LONG).show();
        }

        // Final de tener el JSON en un fichero
         *************************  */


        // ************   Inicio de inserción de registros
        // Suponemos que queremos insertar estos datos. Los datos pueden provenir de la funcionalidad del sistema


        String nombreLocal = "pepeo";
        String apellidoLocal = "MArtínez";

        // Creamos el Listener que nos va a disparar cuando enviemos los datos al servidor.
        Response.Listener<String> respuesta = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // Aqui ponemos lo que queremos que haga con la respuesta (response) del servidor a nuestra peticion
                // Pensemos que estamos dando de alta un nuevo registro.
                // El php envia un array en formato json, por lo tanto tenemos que tratar la información en json
                try {
                    JSONObject jsonRepuesta = new JSONObject(response);
                    boolean ok = jsonRepuesta.getBoolean("success"); // El php nos responde un array con un valor "success". Aquó lo rescatamos- Es un  boolea
                    if (ok) {
                        Toast.makeText(getApplicationContext(), "Inserción en la BBDD Correcta. Verificalo con el PhpAdmin.", LENGTH_LONG).show();
                    } else {
                        AlertDialog.Builder alerta = new AlertDialog.Builder(getApplicationContext());
                        alerta.setMessage("Fallo en el registro")
                                .setNegativeButton("Reintentar", null)
                                .create()
                                .show();
                        Toast.makeText(getApplicationContext(), "ERROR.Algo ha fallado en la inserción!!!!", LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        };
        // Una vez tenemos el Listener ya podemos llamar a la peticion
        Log.i("nico", "1");
        PeticionPost r = PeticionPost.RegistraUsuario(nombreLocal, apellidoLocal, respuesta);
        // El Volley, es quien hace la comunicación, y permite poenr las consultas en una cola. Tenemos que poner la peticion enla cola para que se ejecute
        Log.i("nico", "2");

        RequestQueue cola = Volley.newRequestQueue(this);
        cola.add(r);  // Aquí se ejectua.

        Log.i("nico", "3");


        //*************** Fin de inserción de registros


        // **Inicio Leer Datos del servidor *****************


        // Comprobar la disponibilidad de la Red --> Ojo hay que dar permiso en Manifest.XML
        // Usamos un controlador de conectividad
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo infored = connMgr.getActiveNetworkInfo();  // Depreciated a partir version 29

        if (infored != null && infored.isConnected()) {
            // Hay información y esta conectado
            // Ejecutamos la Tarea Asincrona creada abajo
            // Esta tarea no abre canal
            /*
                try {
                    new JsonTask(this).execute(new URL(dades.get_all_users));
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }*/
            //http://localhost/niko/get_usuarios.php"));
            // OJO!!!! La dirección ha de ser donde tengamos el servidor web. eneste caso al ser local, es la dirección que en cada momento tenga el PC en la red. (ipConfig / All
            // y la dirección IPv4, en mi cado

            PeticionLecturaLista.Lee(this, dades.get_all_users, lista, adaptador); //Versió millorada de JsonTask, funciona amb qualsevol classe que tingui un ArrayAdapter personalitzat que estengui AdaptadorBase

        } else {
            makeText(this, "Error red", LENGTH_LONG).show();
        }
        //*****************************


    }

    public void testLectura(View view) {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo infored = connMgr.getActiveNetworkInfo();  // Depreciated a partir version 29

        if (infored != null && infored.isConnected()) {
            //CODI DE PROVA, S'HAN DE CANVIAR ELS PARAMETRES PER ADAPTAR-HO A EL QUE ES VULGUI
            PeticionLecturaLista.Lee(this, dades.get_all_users, lista, adaptador); //Versió millorada de JsonTask, funciona amb qualsevol classe que tingui un ArrayAdapter personalitzat que estengui AdaptadorBase

        } else {
            makeText(this, "Error red", LENGTH_LONG).show();
        }
    }

    public void testLecturaIndividual(View view) {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo infored = connMgr.getActiveNetworkInfo();  // Depreciated a partir version 29

        if (infored != null && infored.isConnected()) {
            String ruta = dades.get_all_users;
            PeticionLectura.Lee(this, ruta, new RespuestaServidor<Usuario>() {

                @Override
                public void acabarProceso(List<Usuario> output) {
                    muestraUsuario(output.get(0));
                }
            }); //Versió millorada de JsonTask, funciona amb qualsevol classe que tingui un ArrayAdapter personalitzat que estengui AdaptadorBase

        } else {
            makeText(this, "Error red", LENGTH_LONG).show();
        }
    }

    public void muestraUsuario(Usuario usuario) {
        makeText(this, usuario.getUser_name(), LENGTH_SHORT).show();
    }

    /* ***********************************  */

    // Creamos la tasca asincrona

    public class JsonTask extends AsyncTask<URL, Void, List<Usuario>> {
        // Mostramos un ProgressDialog para hacer mas amena la esper (Esto es prescindible--> Hay que inicializar en el onPreExecute())
        private ProgressDialog pd;

        HttpURLConnection con;

        /* Constructor */
        public JsonTask(Activity activity) {

            this.pd = new ProgressDialog(activity); // Necesitamos el Activity en el constuctor para que funcione el ProgessDialog

        }


        @Override
        protected void onPreExecute() {
            // Barra de progreso

            this.pd.setMessage("Obteniendo datos....");
            this.pd.setIndeterminate(false);
            this.pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            this.pd.setCancelable(false);
            this.pd.show();
        }


        @Override
        protected List<Usuario> doInBackground(URL... urls) {
            // DEvolveremos una lista de usuarios que llemos del servicio web
            List<Usuario> usuarios = null;

            // Establecemos conexión sobre la variable global con
            // y sobre el primero de los valores de urls
            // Es obligado que este en un try/catch
            try {

                con = (HttpURLConnection) urls[0].openConnection();

                // Parmatreizamos los tiempos de espera y de lectura. En milisegundos
                con.setConnectTimeout(15000);
                con.setReadTimeout(10000);
                Log.i("nico", urls[0].toString());
                // Ejecturamos conexión y evaluamos resultado

                int codEstadoCon = con.getResponseCode();

                if (codEstadoCon == 200) {

                    // Provocamos un retraso para que se vea efecto del ProgessDialog  ->> mejor no poner Sleeps ni toast  en tareas asincronas,
                    for (int j = 0; j < 9999; j++) {
                        int i = 0;
                        while (i < 9999) {
                            i = i + 1;
                        }

                    }
                    // Tenemos que parsear los datos JSON que nos legan a traves de la conexión
                    InputStream in = new BufferedInputStream(con.getInputStream());
                    // Vamos parseando la entrada usando Gson a usuarios
                    GsonParser parser = new GsonParser();
                    usuarios = parser.leerUsuario(in);

                } else {
                    // Generamos un usuario ficticio para mostrar ERRO
                    usuarios = new ArrayList<>();
                    usuarios.add(new Usuario(-1, "ERROR " + codEstadoCon, " ", " "));

                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                // Desconectamos en cualquera de los casos la conneción
                con.disconnect();
            }
            return usuarios;
        }


        @Override
        protected void onPostExecute(List<Usuario> usuarios) {
            // super.onPostExecute(usuarios);
            // Una vez hemos ejecutado la lectura de los objetos json tenimos que parsearlos al adatador
            if (usuarios != null) {
                //adaptador = new AdaptadorUsuarios(getApplicationContext(), usuarios);
                lista.setAdapter(adaptador);

            } else {
                Toast.makeText(getApplicationContext(), "Error parseando datos", Toast.LENGTH_LONG).show();
            }


            // Quitamos progress
            if (pd.isShowing()) {
                pd.dismiss();
            }

        }
    }


}

