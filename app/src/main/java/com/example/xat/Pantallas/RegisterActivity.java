package com.example.xat.Pantallas;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.example.xat.InteraccionServidor.PeticionPost;
import com.example.xat.R;

import org.json.JSONException;
import org.json.JSONObject;

import static android.widget.Toast.LENGTH_LONG;

public class RegisterActivity extends AppCompatActivity {

    TextView txtError;
    EditText txtUser, txtPassword, txtPasswordConf;
    Button btnRegister;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        final Context context = this;

        txtUser = findViewById(R.id.txtUser);
        txtPassword = findViewById(R.id.txtPassword2);
        txtPasswordConf = findViewById(R.id.txtPassword2);
        btnRegister = findViewById(R.id.btnRegister);
        txtError = findViewById(R.id.txtError);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = txtUser.getText().toString();
                String password = txtPassword.getText().toString();
                String passwordConf = txtPasswordConf.getText().toString();

                if(password.equals(passwordConf)){
                    if(!(username.equals("") || password.equals("") || passwordConf.equals(""))){
                        Response.Listener<String> respuesta = new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // Aqui ponemos lo que queremos que haga con la respuesta (response) del servidor a nuestra peticion
                                // Pensemos que estamos dando de alta un nuevo registro.
                                // El php envia un array en formato json, por lo tanto tenemos que tratar la información en json
                                try {
                                    JSONObject jsonRepuesta = new JSONObject(response);
                                    boolean ok = jsonRepuesta.getBoolean("success"); // El php nos responde un array con un valor "success". Aquó lo rescatamos- Es un  boolea
                                    if (ok) {
                                        Toast.makeText(getApplicationContext(), "Usuari creat correctament", LENGTH_LONG).show();
                                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                        startActivityForResult(intent,2);
                                    } else {
                                        txtUser.setText("");
                                        txtPassword.setText("");
                                        txtPasswordConf.setText("");
                                        txtError.setText("L'usuari ja existeix");
                                        txtError.setTextColor(Color.RED);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        };
                        PeticionPost r = PeticionPost.RegistraUsuario(username, password, respuesta);
                        RequestQueue cola = Volley.newRequestQueue(context);
                        cola.add(r);
                    }else{
                        txtUser.setText("");
                        txtPassword.setText("");
                        txtPasswordConf.setText("");
                        txtError.setText("Cal omplir tots els camps");
                        txtError.setTextColor(Color.RED);
                    }
                }else{
                    txtPassword.setText("");
                    txtPasswordConf.setText("");
                    txtError.setText("Les contrasenyes no coincideixen");
                    txtError.setTextColor(Color.RED);
                }
            }
        });
    }
}
