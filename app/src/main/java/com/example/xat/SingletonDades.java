package com.example.xat;

import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.android.volley.RequestQueue;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

public class SingletonDades extends SQLiteOpenHelper {

    private static SingletonDades sd = null;

    private static final String servidor = "http://virtual406.ies-sabadell.cat/virtual406/Mobils";
    //private static final String servidor = "http://192.168.3.106/virtual406/Mobils"; //TODO Roger: Deberia hacer esto público?

    public Calendar calendari = Calendar.getInstance();

    public static final String nomTaulaUsuaris = "Usuaris";

    public static final String nomTaulaMissatges = "Missatges";

    public static synchronized SingletonDades getInstance(@NonNull Context context) {

        if (sd == null){
            sd = new SingletonDades(context);
        }
        return sd;
    }

    public static synchronized SingletonDades getInstance() throws NullPointerException{ //Por si acaso se necesita una instancia de SingletonDades pero no se tiene acceso al context directamente. La otra función debe haber sido llamada por primera vez con getApplicationContext como contexto
        if (sd == null) throw new NullPointerException("Singleton és null");
        else return sd;
    }

    private SingletonDades(Context context) {
        super(context.getApplicationContext(),"XatLocal", null, 1);

        calendari.clear();
    }


    public static String getServidor() {
        return servidor;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String sqlCreateUsuaris = "CREATE TABLE "+nomTaulaUsuaris+" (user_id INTEGER PRIMARY KEY AUTOINCREMENT, user_name VARCHAR(20) NOT NULL, user_pass VARCHAR(20), last_seen TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, user_image INTEGER(11));"; //TODO Acabar de crear las tablas (esta incluida) para guardar datos localmente
        String sqlCreateMissatges = "CREATE TABLE "+nomTaulaMissatges+" (id_missatge INTEGER PRIMARY KEY AUTOINCREMENT, usuari_remitent INTEGER NOT NULL, usuari_receptor INTEGER NOT NULL, text TEXT NOT NULL, data_hora TIMESTAMP)";

        db.execSQL(sqlCreateUsuaris);
        db.execSQL(sqlCreateMissatges);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        String dropifExists = "DROP TABLE IF EXISTS %s";
        db.execSQL(String.format(dropifExists, nomTaulaUsuaris));
        db.execSQL(String.format(dropifExists, nomTaulaMissatges));
        onCreate(db);
    }


    //rutas de archivos php para no equivocarse accidentalmente al intentar usarlos (y cambiarlos en caso de que sea necesario)

    //Usuari
    public static final String get_all_users = servidor+"/get_usuarios.php";
    public static final String put_user = servidor+"/put_usuario.php";
    public static final String update_user = servidor+"/update_usuario.php";
    public static final String delete_user = servidor+"/delete_usuario.php";

    //Mensajes
    public static final String get_all_messages = servidor+"/get_mensaje.php";
    public static final String post_message = servidor+"/put_mensaje.php";

    //PeticionPost

    public static final String login = servidor+"/login.php";


}
